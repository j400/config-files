#!/usr/bin/env sh

if [[ "$XDG_SESSION_DESKTOP" == "sway" ]]; then
	gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu'
elif [[ "$XDG_SESSION_DESKTOP" == "gnome" ]]; then
	gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:close'
fi
